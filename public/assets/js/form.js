$(function () {
	let formData = {
		'firstname': $('input[name="customer[personalData][firstName]"]').val(),
		'lastname': $('input[name="customer[personalData][lastName]"]').val(),
		'phoneNumber': $('input[name="customer[personalData][phoneNumber][number]"]').val(),
		'streetName': $('input[name="customer[address][street][name]"]').val(),
		'streetNumber': $('input[name="customer[address][street][number]"]').val(),
		'zipCode': $('input[name="customer[address][zipCode]"]').val(),
		'city': $('input[name="customer[address][city]"]').val(),
	};

	let currentStep = 0;

	if (formData.firstname && formData.lastname && formData.phoneNumber) {
		currentStep = 1;

		if (formData.streetName && formData.streetNumber && formData.city && formData.zipCode) {
			currentStep = 2;
		}
	}

	$('form').steps("setStep", currentStep);
})
