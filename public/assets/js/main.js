$(function () {
	$("#wizard").steps({
		headerTag: "h4",
		bodyTag: "section",
		transitionEffect: "fade",
		enableAllSteps: true,
		transitionEffectSpeed: 300,
		labels: {
			next: "Next",
			previous: "Back"
		},
		onStepChanging: function (event, currentIndex, newIndex) {
			var formData = {
				'customer': {
					'firstName': $('input[name="customer[personalData][firstName]"]').val(),
					'lastName': $('input[name="customer[personalData][lastName]"]').val(),
					'number': $('input[name="customer[personalData][phoneNumber][number]"]').val(),
					'countryCode': $('input[name="customer[personalData][phoneNumber][countryCode]"]').val(),
					'streetName': $('input[name="customer[address][street][name]"]').val(),
					'streetNumber': $('input[name="customer[address][street][number]"]').val(),
					'zipCode': $('input[name="customer[address][zipCode]"]').val(),
					'city': $('input[name="customer[address][city]"]').val(),
					'country': $('#customer_address_country').find(':selected').val(),
				}
			};

			$.ajax({
				url: '/saveSession',
				type: 'POST',
				data: formData
			});

			if (newIndex === 1) {
				$('.steps ul').addClass('step-2');
			} else {
				$('.steps ul').removeClass('step-2');
			}
			if (newIndex === 2) {
				$('.steps ul').addClass('step-3');
				$('.actions ul').addClass('mt-7');
			} else {
				$('.steps ul').removeClass('step-3');
				$('.actions ul').removeClass('mt-7');
			}
			return true;
		},
		onFinished: function () {
			$('form').submit();
		},
	});
	// Grid
	$('.grid .grid-item').click(function () {
		$('.grid .grid-item').removeClass('active');
		$(this).addClass('active');
	})

	$.fn.steps.setStep = function (step) {
		let currentIndex = $(this).steps('getCurrentIndex');
		for (let i = 0; i < Math.abs(step - currentIndex); i++) {
			if (step > currentIndex) {
				$(this).steps('next');
			} else {
				$(this).steps('previous');
			}
		}
	};
});

