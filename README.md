## This project was created with the following technologies

- Symfony 5.2
- MySQL
- Docker
- Nginx

## To install follow the instructions

- `docker-compose up -d --build`
- `docker-compose exec --user 1000:1000 php fish`
- `composer install`
- `php bin/console d:m:m`


After that you will be able to access the project on http://localhost

## Tests

To run the tests you need to

1. Get into the container

- `docker-compose exec --user 1000:1000 php fish`

2. Run

- `php bin/phpunit`


You can find my DB structure inside app > migrations	

To structure my code I take inspiration from DDD ( I don’t like to say that I follow DDD because is a complex topic and is quite hard to strictly follow it, like scrum)
						
#### Describe possible performance optimizations for your Code.
The process to register the user is done synchronously which could bring issues in terms of loading/performance as it depends on the third party api, one possible optimization would be to bring this process to a more async way where the flow doesn’t break and the request can be retried.

#### Which things could be done better, than you’ve done it? 
Besides what I have described above, the way of dealing with sessions could be done better or not be done at all, because one possible solution would be to go for cookies, it would depend on the need.
More tests can be added as well, like functional tests to the controllers.

 							
