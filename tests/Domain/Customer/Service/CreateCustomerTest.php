<?php

declare(strict_types=1);

namespace App\Tests\Domain\Customer\Service;

use App\Domain\Customer\Service\CreateCustomer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class CreateCustomerTest extends TestCase
{
    public function testCreateCustomerWhenNoSessionIsSet(): void
    {
        $session = new Session(new MockArraySessionStorage());

        $service = new CreateCustomer($session);
        $customer = $service->execute();

        self::assertNull($customer->getPaymentData());
        self::assertNull($customer->getAddress());
        self::assertNull($customer->getPersonalData());
    }

    public function testCreateCustomerWhenSessionIsSet(): void
    {
        $customer = [
            'firstName' => 'Firstname',
            'lastName' => 'Lastname',
        ];

        $session = new Session(new MockArraySessionStorage());
        $session->set('customer', $customer);

        $service = new CreateCustomer($session);
        $customer = $service->execute();

        self::assertSame('Firstname', $customer->getPersonalData()->getFirstName()->asString());
        self::assertSame('Lastname', $customer->getPersonalData()->getLastName()->asString());
    }
}
