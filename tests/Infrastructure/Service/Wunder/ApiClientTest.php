<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\Service\Wunder;

use App\Domain\Customer\Model\Customer;
use App\Domain\Customer\Model\PaymentData\Iban;
use App\Domain\Customer\Model\PaymentData\PaymentData;
use App\Infrastructure\Service\Wunder\ApiClient;
use App\Infrastructure\Service\Wunder\ApiUrl;
use App\Infrastructure\Service\Wunder\Exception\ApiResponse;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class ApiClientTest extends TestCase
{
    public function testExceptionIsThrownIf200IsNotReturnedWhenRegisterCustomer(): void
    {
        $httpClient = new MockHttpClient(new MockResponse('', ['http_code' => 400]));

        $apiUrl = new ApiUrl('base_url', 'default');

        $client = new ApiClient($httpClient, $apiUrl);

        $paymentData = $this->createMock(PaymentData::class);
        $paymentData->expects(self::once())->method('getIban')->willReturn(Iban::fromString('DE1239839283'));
        $paymentData->expects(self::once())->method('getAccountOwner')->willReturn('account owner');

        $customer = $this->createMock(Customer::class);
        $customer->expects(self::once())->method('getId')->willReturn(Uuid::uuid4());
        $customer->expects(self::atLeast(2))->method('getPaymentData')->willReturn($paymentData);

        $this->expectExceptionMessage('Failed when creating customer');
        $this->expectException(ApiResponse::class);

        $client->registerCustomer($customer);
    }

    public function testReturns200AndRegisterCustomer(): void
    {
        $httpClient = new MockHttpClient(
            new MockResponse(
                json_encode(['paymentDataId' => 'payment-id'], JSON_THROW_ON_ERROR),
                [
                    'http_code' => 200,
                ]
            )
        );

        $apiUrl = new ApiUrl('https://base_url', 'default');

        $client = new ApiClient($httpClient, $apiUrl);

        $paymentData = $this->createMock(PaymentData::class);
        $paymentData->expects(self::once())->method('getIban')->willReturn(Iban::fromString('DE1239839283'));
        $paymentData->expects(self::once())->method('getAccountOwner')->willReturn('account owner');

        $customer = $this->createMock(Customer::class);
        $customer->expects(self::once())->method('getId')->willReturn(Uuid::uuid4());
        $customer->expects(self::atLeast(2))->method('getPaymentData')->willReturn($paymentData);

        $customerRegistrationResponse = $client->registerCustomer($customer);

        self::assertEquals('payment-id', $customerRegistrationResponse->paymentDataId());
    }

}
