<?php

declare(strict_types=1);

namespace App\Tests\Infrastructure\Domain\Customer\Service;

use App\Domain\Customer\Model\Customer;
use App\Domain\Customer\Model\PaymentData\Iban;
use App\Domain\Customer\Model\PaymentData\PaymentData;
use App\Infrastructure\Domain\Customer\Service\RegisterCustomer;
use App\Infrastructure\Repository\Customer\CustomerRepository;
use App\Infrastructure\Service\Wunder\ApiClient;
use App\Infrastructure\Service\Wunder\Exception\ApiResponse;
use App\Infrastructure\Service\Wunder\Payload\CustomerRegistrationResponse;
use PHPUnit\Framework\TestCase;

class RegisterCustomerTest extends TestCase
{
    public function testRegisterCustomer(): void
    {
        $client = $this->createMock(ApiClient::class);
        $client->expects(self::once())->method('registerCustomer')->willReturn(
            CustomerRegistrationResponse::fromPayload(['paymentDataId' => 'payment-id'])
        );

        $customer = $this->createMock(Customer::class);
        $customer->expects(self::atLeastOnce())->method('getPaymentData')->willReturn(
            new PaymentData('owner', Iban::fromString('DE123456789'))
        );

        $repository = $this->createMock(CustomerRepository::class);
        $repository->expects(self::exactly(2))->method('add')->with($customer);

        $service = new RegisterCustomer($client, $repository);
        $service->execute($customer);

        self::assertSame('payment-id', $customer->getPaymentData()->getPaymentDataId());
    }

    public function testRegisterCustomerThrowsExceptionWhenWunderApiReturnsError(): void
    {
        $client = $this->createMock(ApiClient::class);
        $client->expects(self::once())->method('registerCustomer')->willThrowException(
            ApiResponse::forInvalidResponse('invalid response')
        );

        $customer = $this->createMock(Customer::class);

        $repository = $this->createMock(CustomerRepository::class);
        $repository->expects(self::once())->method('add')->with($customer);

        $this->expectException(ApiResponse::class);
        $this->expectExceptionMessage('invalid response');

        $service = new RegisterCustomer($client, $repository);
        $service->execute($customer);
    }
}
