<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Customer\Form\PaymentData;

use App\Domain\Customer\Model\PaymentData\Iban;
use App\Infrastructure\Domain\Common\Form\ScalarType;

class IbanType extends ScalarType
{
    protected function modelClass(): string
    {
        return Iban::class;
    }

    /**
     * {@inheritDoc}
     */
    protected function createModel($value): Iban
    {
        return Iban::fromString($value);
    }
}
