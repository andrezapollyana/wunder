<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Customer\Form\PaymentData;

use App\Domain\Customer\Model\PaymentData\PaymentData;
use App\Domain\Customer\Model\PaymentData\PaymentDataInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PaymentDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('accountOwner', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('iban', IbanType::class, ['constraints' => [new NotBlank()]]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault(
            'empty_data',
            static function (FormInterface $form): ?PaymentDataInterface {
                try {
                    return new PaymentData(
                        $form['accountOwner']->getData(),
                        $form['iban']->getData(),
                    );
                } catch (\Throwable $e) {
                    return null;
                }
            }
        );
        $resolver->setDefault('data_class', PaymentData::class);
    }
}
