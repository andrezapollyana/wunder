<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Customer\Form;

use App\Domain\Customer\Model\Customer;
use App\Infrastructure\Domain\Address\Form\AddressType;
use App\Infrastructure\Domain\Customer\Form\PaymentData\PaymentDataType;
use App\Infrastructure\Domain\Customer\Form\PersonalData\PersonalDataType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('personalData', PersonalDataType::class)
            ->add('paymentData', PaymentDataType::class)
            ->add('address', AddressType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Customer::class,
            ]
        );
    }
}
