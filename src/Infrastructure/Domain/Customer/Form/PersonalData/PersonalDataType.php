<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Customer\Form\PersonalData;

use App\Domain\Customer\Model\PersonalData\PersonalData;
use App\Infrastructure\Domain\Common\Form\FirstNameType;
use App\Infrastructure\Domain\Common\Form\LastNameType;
use App\Infrastructure\Domain\Common\Form\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PersonalDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'firstName',
                FirstNameType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                    ],
                    'label' => 'Firstname',
                ],
            )
            ->add('lastName', LastNameType::class, ['constraints' => [new NotBlank()]])
            ->add(
                'phoneNumber',
                PhoneNumberType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault(
            'empty_data',
            static function (FormInterface $form): ?PersonalData {
                try {
                    return new PersonalData(
                        $form['firstName']->getData(),
                        $form['lastName']->getData(),
                        $form['phoneNumber']->getData()
                    );
                } catch (\Throwable $e) {
                    return null;
                }
            }
        );
        $resolver->setDefault('data_class', PersonalData::class);
    }
}
