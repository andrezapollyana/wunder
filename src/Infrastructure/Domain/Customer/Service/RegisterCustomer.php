<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Customer\Service;

use App\Domain\Customer\Model\Customer;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use App\Infrastructure\Service\Wunder\ApiClient;

class RegisterCustomer
{
    private ApiClient $apiClient;

    private CustomerRepositoryInterface $repository;

    public function __construct(ApiClient $apiClient, CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->apiClient = $apiClient;
    }

    public function execute(Customer $customer): string
    {
        $this->repository->add($customer);

        $response = $this->apiClient->registerCustomer($customer);
        $customer->getPaymentData()->setPaymentDataId($response->paymentDataId());

        $this->repository->add($customer);

        return $response->paymentDataId();
    }
}
