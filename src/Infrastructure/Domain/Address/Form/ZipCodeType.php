<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Address\Form;

use App\Domain\Address\Model\ZipCode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Throwable;

use function is_string;

final class ZipCodeType extends AbstractType implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    public function getParent(): string
    {
        return TextType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        if (! $value instanceof ZipCode) {
            throw new UnexpectedTypeException($value, ZipCode::class);
        }

        return (string) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if ($value === '' || $value === null) {
            return null;
        }

        if (! is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        try {
            return ZipCode::fromString($value);
        } catch (Throwable $exception) {
            throw new TransformationFailedException($exception->getMessage());
        }
    }
}
