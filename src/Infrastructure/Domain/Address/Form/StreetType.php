<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Address\Form;

use App\Domain\Address\Model\Street;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Throwable;

use function is_array;

final class StreetType extends AbstractType implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Address'])
            ->add('number', TextType::class, ['required' => false, 'label' => 'House number']);

        $builder->addModelTransformer($this);
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value === '' || $value === null || $value === []) {
            return null;
        }

        if (! $value instanceof Street) {
            throw new UnexpectedTypeException($value, Street::class);
        }

        return [
            'name' => $value->name(),
            'number' => $value->number(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if ($value === '' || $value === null || $value === [] || $value === ['name' => null, 'number' => null]) {
            return null;
        }

        if ($value['name'] === null) {
            throw new TransformationFailedException('Street name is missing');
        }

        if (! is_array($value)) {
            throw new UnexpectedTypeException($value, 'array');
        }

        try {
            return Street::fromString($value['name'], $value['number']);
        } catch (Throwable $exception) {
            throw new TransformationFailedException($exception->getMessage());
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('label', false);
    }
}
