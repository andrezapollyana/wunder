<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Address\Form;

use App\Domain\Address\Model\Country;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\CountryType as SymfonyCountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Throwable;

use function is_string;

final class CountryType extends SymfonyCountryType implements DataTransformerInterface
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('preferred_choices', ['DE']);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value === null) {
            $value = Country::fromIso3166Alpha2('DE');
        }

        if (! $value instanceof Country) {
            throw new UnexpectedTypeException($value, Country::class);
        }

        try {
            return $value;
        } catch (Throwable $exception) {
            throw new TransformationFailedException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if ($value === '' || $value === null || $value === []) {
            return null;
        }

        if (! is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        try {
            return Country::fromIso3166Alpha2($value);
        } catch (Throwable $exception) {
            throw new TransformationFailedException($exception->getMessage());
        }
    }
}
