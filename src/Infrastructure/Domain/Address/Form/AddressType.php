<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Address\Form;

use App\Domain\Address\Model\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Throwable;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('street', StreetType::class, ['error_bubbling' => false])
            ->add(
                'zipCode',
                ZipCodeType::class,
                [
                    'error_bubbling' => false,
                    'label' => 'Zipcode',
                ]
            )
            ->add(
                'city',
                CityType::class,
                [
                    'label' => 'City',
                ]
            )
            ->add(
                'country',
                CountryType::class,
                [
                    'error_bubbling' => false,
                    'placeholder' => '',
                    'label' => 'Country',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Address::class,
                'empty_data' => static function (FormInterface $form): ?Address {
                    try {
                        return new Address(
                            $form->get('street')->getData(),
                            $form->get('zipCode')->getData(),
                            $form->get('city')->getData(),
                            $form->get('country')->getData(),
                        );
                    } catch (Throwable $e) {
                        return null;
                    }
                },
            ]
        );
    }
}
