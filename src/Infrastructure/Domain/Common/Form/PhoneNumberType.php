<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Common\Form;

use App\Domain\Common\PhoneNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Throwable;

final class PhoneNumberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('countryCode', HiddenType::class)
            ->add('number', TelType::class, ['label' => 'Phone number']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => PhoneNumber::class,
                'empty_data' => static function (FormInterface $form): ?PhoneNumber {
                    try {
                        return new PhoneNumber(
                            (int) $form->get('countryCode')->getData(),
                            (int) $form->get('number')->getData(),
                        );
                    } catch (Throwable $e) {
                        return null;
                    }
                },
            ]
        );
    }
}
