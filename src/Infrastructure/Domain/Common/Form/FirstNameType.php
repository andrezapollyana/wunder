<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Common\Form;

use App\Domain\Common\FirstName;

final class FirstNameType extends ScalarType
{
    protected function modelClass(): string
    {
        return FirstName::class;
    }

    /**
     * {@inheritDoc}
     */
    protected function createModel($value): FirstName
    {
        return FirstName::fromString($value);
    }
}
