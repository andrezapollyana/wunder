<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Common\Form;

use RuntimeException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Throwable;

use function get_class;
use function is_scalar;
use function method_exists;
use function sprintf;

abstract class ScalarType extends AbstractType implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    final public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer($this);
    }

    public function getParent(): string
    {
        return TextType::class;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'error_bubbling' => false,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        $modelClass = $this->modelClass();

        if (! $value instanceof $modelClass) {
            throw new UnexpectedTypeException($value, $modelClass);
        }

        return $this->stringify($value);
    }

    protected function stringify(object $value): string
    {
        if (method_exists($value, '__toString')) {
            return (string) $value;
        }

        if (method_exists($value, 'asString')) {
            return $value->asString();
        }

        throw new RuntimeException(
            sprintf('Can not transform type "%s" to string.', get_class($value))
        );
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if ($value === null || $value === '') {
            return null;
        }

        if (! is_scalar($value)) {
            throw new UnexpectedTypeException($value, 'scalar');
        }

        try {
            return $this->createModel($value);
        } catch (Throwable $exception) {
            throw new TransformationFailedException(
                sprintf(
                    'Could not transform string to %s',
                    $this->modelClass()
                ),
                0,
                $exception,
                $exception->getMessage()
            );
        }
    }

    abstract protected function modelClass(): string;

    /**
     * @param mixed $value The value from the transformed representation.
     *
     * @return mixed The transformed value.
     */
    abstract protected function createModel($value);
}
