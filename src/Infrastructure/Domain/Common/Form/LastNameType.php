<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Common\Form;

use App\Domain\Common\LastName;

final class LastNameType extends ScalarType
{
    protected function modelClass(): string
    {
        return LastName::class;
    }

    /**
     * {@inheritDoc}
     */
    protected function createModel($value): LastName
    {
        return LastName::fromString($value);
    }
}
