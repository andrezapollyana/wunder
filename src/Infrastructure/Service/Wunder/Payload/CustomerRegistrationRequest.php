<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Wunder\Payload;

final class CustomerRegistrationRequest implements \JsonSerializable
{
    private string $customerId;

    private string $iban;

    private string $owner;

    public function __construct(string $customerId, string $iban, string $owner)
    {
        $this->customerId = $customerId;
        $this->iban = $iban;
        $this->owner = $owner;
    }

    public function jsonSerialize(): array
    {
        return [
            'customerId' => $this->customerId,
            'iban' => $this->iban,
            'owner' => $this->owner,
        ];
    }
}
