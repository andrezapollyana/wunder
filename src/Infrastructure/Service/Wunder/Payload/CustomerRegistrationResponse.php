<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Wunder\Payload;

final class CustomerRegistrationResponse
{
    private string $paymentDataId;

    private function __construct(string $paymentDataId)
    {
        $this->paymentDataId = $paymentDataId;
    }

    /**
     * @param mixed[] $payload
     */
    public static function fromPayload(array $payload): self
    {
        return new self($payload['paymentDataId']);
    }

    public function paymentDataId(): string
    {
        return $this->paymentDataId;
    }
}
