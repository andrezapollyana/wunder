<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Wunder;

final class ApiUrl
{
    private string $root;

    public function __construct(string $host, string $baseUrl)
    {
        $this->root = \sprintf('%s/%s/', $host, $baseUrl);
    }

    public function getUri(string $path): string
    {
        return $this->root . \ltrim($path, '/');
    }
}
