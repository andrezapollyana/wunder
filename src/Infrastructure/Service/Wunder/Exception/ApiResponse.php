<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Wunder\Exception;

use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

final class ApiResponse extends \RuntimeException
{
    private ?ResponseInterface $response;

    private function __construct(
        string $message,
        ?ResponseInterface $response,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, 0, $previous);
        $this->response = $response;
    }

    public static function forInvalidResponse(
        string $message,
        ResponseInterface $response = null,
        ?Throwable $previous = null
    ): self {
        return new self($message, $response, $previous);
    }

    public function response(): ResponseInterface
    {
        return $this->response;
    }
}
