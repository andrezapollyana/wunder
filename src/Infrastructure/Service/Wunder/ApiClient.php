<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Wunder;

use App\Domain\Customer\Model\Customer;
use App\Infrastructure\Service\Wunder\Exception\ApiResponse;
use App\Infrastructure\Service\Wunder\Payload\CustomerRegistrationRequest;
use App\Infrastructure\Service\Wunder\Payload\CustomerRegistrationResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClient
{
    private const API_PATH = 'wunderfleet-recruiting-backend-dev-save-payment-data';

    private HttpClientInterface $httpClient;

    private ApiUrl $apiUrl;

    public function __construct(HttpClientInterface $httpClient, ApiUrl $apiUrl)
    {
        $this->httpClient = $httpClient;
        $this->apiUrl = $apiUrl;
    }

    public function registerCustomer(Customer $customer): CustomerRegistrationResponse
    {
        try {
            $requestPayload = new CustomerRegistrationRequest(
                $customer->getId()->toString(),
                $customer->getPaymentData()->getIban()->asString(),
                $customer->getPaymentData()->getAccountOwner()
            );

            $response = $this->httpClient->request(
                'POST',
                $this->apiUrl->getUri(self::API_PATH),
                [
                    'json' => $requestPayload,
                ]
            );

            if ($response->getStatusCode() === Response::HTTP_OK) {
                return CustomerRegistrationResponse::fromPayload($response->toArray());
            }

            throw ApiResponse::forInvalidResponse('Failed when creating customer', $response);
        } catch (\Throwable $exception) {
            throw ApiResponse::forInvalidResponse('Failed when creating customer', null, $exception);
        }
    }
}
