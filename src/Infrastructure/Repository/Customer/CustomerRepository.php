<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Customer;

use App\Domain\Customer\Model\Customer;
use App\Domain\Customer\Model\CustomerInterface;
use App\Domain\Customer\Repository\CustomerRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CustomerRepository extends ServiceEntityRepository implements CustomerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function add(CustomerInterface $customer): void
    {
        $this->_em->persist($customer);
        $this->_em->flush();
    }
}
