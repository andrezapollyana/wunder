<?php

declare(strict_types=1);

namespace App\Util;

class Helper
{
    public static function isValidKey(string $key, array $array): bool
    {
        return isset($array[$key]) && $array[$key] !== '';
    }
}
