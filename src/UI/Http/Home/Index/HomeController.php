<?php

declare(strict_types=1);

namespace App\UI\Http\Home\Index;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    public function __invoke(Request $request): Response
    {
        return $this->render('home/index.html.twig');
    }
}
