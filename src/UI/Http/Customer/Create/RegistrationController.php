<?php

declare(strict_types=1);

namespace App\UI\Http\Customer\Create;

use App\Domain\Customer\Service\CreateCustomer;
use App\Infrastructure\Domain\Customer\Form\CustomerType;
use App\Infrastructure\Domain\Customer\Service\RegisterCustomer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends AbstractController
{
    private RegisterCustomer $registerCustomer;

    private CreateCustomer $createCustomer;

    public function __construct(
        RegisterCustomer $registerCustomer,
        CreateCustomer $createCustomer
    ) {
        $this->registerCustomer = $registerCustomer;
        $this->createCustomer = $createCustomer;
    }

    public function __invoke(Request $request): Response
    {
        $customer = $this->createCustomer->execute();

        $form = $this->createForm(CustomerType::class, $customer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $paymentDataId = $this->registerCustomer->execute($customer);

                return $this->render(
                    'customer/finish.html.twig',
                    [
                        'paymentId' => $paymentDataId,
                    ]
                );
            } catch (\Exception $exception) {
                $this->addFlash('error', 'There was an error while registering');
            }
        }

        return $this->render(
            'customer/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
