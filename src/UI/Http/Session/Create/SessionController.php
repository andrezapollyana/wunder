<?php

declare(strict_types=1);

namespace App\UI\Http\Session\Create;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionController extends AbstractController
{
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function __invoke(Request $request) : Response
    {
        foreach ($request->request->all() as $index => $value) {
            $this->session->set($index, $value);
        }

        return new JsonResponse();
    }
}
