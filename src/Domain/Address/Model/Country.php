<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use RuntimeException;
use Symfony\Component\Intl\Countries;

use Webmozart\Assert\Assert;
use function sprintf;
use function strtoupper;

final class Country
{
    private string $code;

    public static function fromIso3166Alpha2(string $code): self
    {
        Assert::length($code, 2);

        $code = strtoupper($code);

        $countries = Countries::getNames();
        Assert::keyExists($countries, $code, 'No country with code "%s" exists.');

        return new self($code);
    }

    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public function name(?string $displayLocale = null): string
    {
        $name = Countries::getName($this->code, $displayLocale);

        if ($name === null) {
            throw new RuntimeException(sprintf('Could not get country "%s" name.', $this->code));
        }

        return $name;
    }

    public function __toString(): string
    {
        return $this->code;
    }
}
