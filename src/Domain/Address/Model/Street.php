<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use Webmozart\Assert\Assert;

use function trim;

final class Street
{
    private string $name;

    private ?string $number;

    private function __construct(string $name, ?string $number = null)
    {
        $this->name   = $name;
        $this->number = $number;
    }

    public static function fromString(string $name, ?string $number = null): self
    {
        $errorMessage = 'The value is not a valid street name. Got: %s';
        Assert::notEmpty($name, $errorMessage);

        if ($number !== null) {
            $errorMessage = 'The value is not a valid street number. Got: %s';
            Assert::notEmpty($number, $errorMessage);
            Assert::notWhitespaceOnly($number, $errorMessage);
        }

        return new self($name, $number);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function number(): ?string
    {
        return $this->number;
    }

    public function __toString(): string
    {
        return trim($this->name . ' ' . $this->number);
    }
}
