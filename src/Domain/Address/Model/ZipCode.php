<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use Webmozart\Assert\Assert;

final class ZipCode
{
    private string $zipCode;

    private function __construct(string $zipCode)
    {
        $this->zipCode = $zipCode;
    }

    public static function fromString(string $zipCode): self
    {
        $errorMessage = 'The value is not a valid zip code. Got: %s';
        Assert::notEmpty($zipCode, $errorMessage);
        Assert::notWhitespaceOnly($zipCode, $errorMessage);

        return new self($zipCode);
    }

    public function asString(): string
    {
        return $this->zipCode;
    }

    public function __toString(): string
    {
        return $this->zipCode;
    }
}
