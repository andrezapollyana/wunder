<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

final class Coordinates
{
    private float $latitude;

    private float $longitude;

    public function __construct(float $lat, float $long)
    {
        $this->latitude  = $lat;
        $this->longitude = $long;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }
}
