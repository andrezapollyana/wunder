<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use Ramsey\Uuid\UuidInterface;

interface AddressInterface
{
    public function getId(): UuidInterface;

    public function getStreet(): ?Street;

    public function setStreet(Street $street): void;

    public function getZipCode(): ?ZipCode;

    public function setZipCode(ZipCode $zipCode): void;

    public function getCity(): ?City;

    public function setCity(City $city): void;

    public function getCountry(): ?Country;

    public function setCountry(Country $country): void;
}
