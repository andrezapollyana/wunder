<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use Webmozart\Assert\Assert;

final class City
{
    private string $name;

    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromString(string $name): self
    {
        $errorMessage = 'The value is not a valid street name. Got: %s';
        Assert::notEmpty($name, $errorMessage);

        return new self($name);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
