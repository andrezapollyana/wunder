<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use Webmozart\Assert\Assert;

use function sprintf;

final class StreetName
{
    private const LENGTH = 256;

    private string $value;

    public function __construct(string $value)
    {
        Assert::notWhitespaceOnly($value);
        Assert::maxLength(
            $value,
            self::LENGTH,
            sprintf('This value is too long. It should have %s characters or less.', self::LENGTH)
        );

        $this->value = $value;
    }

    public static function fromString(string $streetName): self
    {
        return new self($streetName);
    }

    public function asString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
