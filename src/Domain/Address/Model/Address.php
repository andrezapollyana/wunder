<?php

declare(strict_types=1);

namespace App\Domain\Address\Model;

use App\Util\Helper;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Address implements AddressInterface
{
    private UuidInterface $id;

    private ?Street $street;

    private ?ZipCode $zipCode;

    private ?City $city;

    private ?Country $country;

    public function __construct(
        ?Street $street,
        ?ZipCode $zipCode,
        ?City $city,
        ?Country $country
    ) {
        $this->id = Uuid::uuid4();
        $this->street = $street;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->country = $country;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public static function fromArray(array $address): self
    {
        return new Address(
            Helper::isValidKey('streetName', $address) ? Street::fromString(
                $address['streetName'],
                $address['streetNumber'] ?? null
            ) : null,
            Helper::isValidKey('zipCode', $address) ? ZipCode::fromString($address['zipCode']) : null,
            Helper::isValidKey('city', $address) ? City::fromString($address['city']) : null,
            Helper::isValidKey('country', $address) ? Country::fromIso3166Alpha2($address['country']) : null,
        );
    }

    public function setIdFromString(string $id): void
    {
        $this->id = Uuid::fromString($id);
    }

    public function getStreet(): ?Street
    {
        return $this->street;
    }

    public function setStreet(Street $street): void
    {
        $this->street = $street;
    }

    public function getZipCode(): ?ZipCode
    {
        return $this->zipCode;
    }

    public function setZipCode(ZipCode $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): void
    {
        $this->city = $city;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }
}
