<?php

declare(strict_types=1);

namespace App\Domain\Customer\Repository;

use App\Domain\Customer\Model\CustomerInterface;

interface CustomerRepositoryInterface
{
    public function add(CustomerInterface $customer): void;
}
