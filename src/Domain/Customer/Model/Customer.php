<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model;

use App\Domain\Address\Model\Address;
use App\Domain\Address\Model\AddressInterface;
use App\Domain\Customer\Model\PaymentData\PaymentDataInterface;
use App\Domain\Customer\Model\PersonalData\PersonalData;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Customer implements CustomerInterface
{
    private UuidInterface $id;

    private PersonalData $personalData;

    private ?AddressInterface $address;

    private PaymentDataInterface $paymentData;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->address = null;
    }

    public static function fromArray(array $customer): self
    {
        $self = new self();
        $self->setPersonalData(PersonalData::fromArray($customer));
        $self->setAddress(Address::fromArray($customer));

        return $self;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getAddress(): ?AddressInterface
    {
        return $this->address;
    }

    public function setAddress(AddressInterface $address): void
    {
        $this->address = $address;
    }

    public function getPaymentData(): ?PaymentDataInterface
    {
        return $this->paymentData ?? null;
    }

    public function setPaymentData(PaymentDataInterface $paymentData): void
    {
        $this->paymentData = $paymentData;
    }

    public function getPersonalData(): ?PersonalData
    {
        return $this->personalData ?? null;
    }

    public function setPersonalData(PersonalData $personalData): void
    {
        $this->personalData = $personalData;
    }
}
