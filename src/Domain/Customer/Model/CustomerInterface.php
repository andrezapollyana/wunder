<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model;

use App\Domain\Address\Model\AddressInterface;
use App\Domain\Customer\Model\PaymentData\PaymentDataInterface;
use App\Domain\Customer\Model\PersonalData\PersonalData;
use Ramsey\Uuid\UuidInterface;

interface CustomerInterface
{
    public function getId(): UuidInterface;

    public function getAddress(): ?AddressInterface;

    public function setAddress(AddressInterface $address): void;

    public function getPaymentData(): ?PaymentDataInterface;

    public function setPaymentData(PaymentDataInterface $paymentData): void;

    public function getPersonalData(): ?PersonalData;

    public function setPersonalData(PersonalData $personalData): void;
}
