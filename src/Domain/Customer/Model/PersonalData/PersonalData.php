<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model\PersonalData;

use App\Domain\Common\FirstName;
use App\Domain\Common\LastName;
use App\Domain\Common\PhoneNumber;
use App\Util\Helper;

class PersonalData
{
    private ?FirstName $firstName;

    private ?LastName $lastName;

    private ?PhoneNumber $phoneNumber;

    public function __construct(?FirstName $firstName, ?LastName $lastName, ?PhoneNumber $phoneNumber)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phoneNumber = $phoneNumber;
    }

    public static function fromArray(array $personalData): self
    {
        return new self(
            Helper::isValidKey('firstName', $personalData) ? FirstName::fromString($personalData['firstName']) : null,
            Helper::isValidKey('lastName', $personalData) ? LastName::fromString($personalData['lastName']) : null,
            Helper::isValidKey('number', $personalData) ? PhoneNumber::fromString(
                \sprintf('%s %s', $personalData['countryCode'], $personalData['number'])
            ) : null
        );
    }

    public function setFirstName(FirstName $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): ?FirstName
    {
        return $this->firstName;
    }

    public function setLastName(LastName $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getLastName(): ?LastName
    {
        return $this->lastName;
    }

    public function setPhoneNumber(PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }
}
