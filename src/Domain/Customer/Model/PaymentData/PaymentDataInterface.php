<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model\PaymentData;

interface PaymentDataInterface
{
    public function getId();

    public function getAccountOwner(): string;

    public function setAccountOwner(string $accountOwner): void;

    public function getIban(): Iban;

    public function setIban(Iban $iban): void;

    public function setPaymentDataId(?string $paymentDataId): void;

    public function getPaymentDataId(): ?string;
}
