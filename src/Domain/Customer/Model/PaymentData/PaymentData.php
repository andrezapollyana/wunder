<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model\PaymentData;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class PaymentData implements PaymentDataInterface
{
    private UuidInterface $id;

    private string $accountOwner;

    private Iban $iban;

    private ?string $paymentDataId;

    public function __construct(string $accountOwner, Iban $iban)
    {
        $this->id = Uuid::uuid4();
        $this->accountOwner = $accountOwner;
        $this->iban = $iban;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getAccountOwner(): string
    {
        return $this->accountOwner;
    }

    public function setAccountOwner(string $accountOwner): void
    {
        $this->accountOwner = $accountOwner;
    }

    public function getIban(): Iban
    {
        return $this->iban;
    }

    public function setIban(Iban $iban): void
    {
        $this->iban = $iban;
    }

    public function setPaymentDataId(?string $paymentDataId): void
    {
        $this->paymentDataId = $paymentDataId;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentDataId;
    }
}
