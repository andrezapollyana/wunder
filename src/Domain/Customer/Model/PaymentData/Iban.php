<?php

declare(strict_types=1);

namespace App\Domain\Customer\Model\PaymentData;

use Webmozart\Assert\Assert;

final class Iban
{
    private const MAX_LENGTH = 31;

    private string $value;

    public function __construct(string $value)
    {
        Assert::notWhitespaceOnly($value);
        Assert::alpha(substr($value, 0, 2), 'Your Iban looks invalid');
        Assert::maxLength(
            $value,
            self::MAX_LENGTH,
            sprintf('This value is too long. It should have %s characters or less.', self::MAX_LENGTH)
        );

        $this->value = $value;
    }

    public static function fromString(string $firstName): self
    {
        return new self($firstName);
    }

    public function asString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
