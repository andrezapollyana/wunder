<?php

declare(strict_types=1);

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Model\Customer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CreateCustomer
{
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function execute(): Customer
    {
        $customer = $this->session->get('customer', new Customer());

        if ($customer instanceof Customer) {
            return $customer;
        }

        return Customer::fromArray($customer);
    }
}
