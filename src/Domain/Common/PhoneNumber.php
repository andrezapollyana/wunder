<?php

declare(strict_types=1);

namespace App\Domain\Common;

use function explode;
use function sprintf;

class PhoneNumber
{
    private ?int $countryCode;

    private ?int $number;

    public function __construct(?int $countryCode, ?int $number)
    {
        $this->countryCode = $countryCode;
        $this->number      = $number;
    }

    public function asString(): string
    {
        return sprintf('%d %d', $this->countryCode, $this->number);
    }

    public static function fromString(string $phoneNumber): self
    {
        [$code, $number] = explode(' ', $phoneNumber);

        return new self((int) $code, (int) $number);
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getCountryCode(): ?int
    {
        return $this->countryCode;
    }

    public function setCountryCode(?int $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    public function setNumber(?int $number): void
    {
        $this->number = $number;
    }

    public function __toString()
    {
        return $this->asString();
    }
}
