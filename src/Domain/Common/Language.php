<?php

declare(strict_types=1);

namespace App\Domain\Common;

use JsonSerializable;
use Webmozart\Assert\Assert;
use function sprintf;

final class Language implements JsonSerializable
{
    private const LENGTH = 64;

    private string $value;

    public function __construct(string $value)
    {
        Assert::notWhitespaceOnly($value);
        Assert::maxLength(
            $value,
            self::LENGTH,
            sprintf('This value is too long. It should have %s characters or less.', self::LENGTH)
        );
        Assert::inArray($value, \App\Domain\PropertyOwner\Model\Language::getValues());

        $this->value = $value;
    }

    public static function fromString(string $firstName) : self
    {
        return new self($firstName);
    }

    public function __toString() : string
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}
