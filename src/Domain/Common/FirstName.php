<?php

declare(strict_types=1);

namespace App\Domain\Common;

use Webmozart\Assert\Assert;
use function sprintf;

final class FirstName
{
    private const LENGTH = 64;

    private string $value;

    public function __construct(string $value)
    {
        Assert::notWhitespaceOnly($value);
        Assert::maxLength(
            $value,
            self::LENGTH,
            sprintf('This value is too long. It should have %s characters or less.', self::LENGTH)
        );

        $this->value = $value;
    }

    public static function fromString(string $firstName): self
    {
        return new self($firstName);
    }

    public function asString(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }
}
