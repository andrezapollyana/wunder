<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210131150820 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Initial schema';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE address (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        street_name VARCHAR(255) NOT NULL, street_number VARCHAR(16) DEFAULT NULL, 
        zip_code_zip_code VARCHAR(16) NOT NULL, country_code CHAR(2) NOT NULL, 
        city_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        payment_data_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', 
        address_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', 
        personal_data_first_name VARCHAR(255) NOT NULL, personal_data_last_name VARCHAR(255) NOT NULL, 
        personal_data_phone_number VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_81398E092EBCAFD6 (payment_data_id), 
        UNIQUE INDEX UNIQ_81398E09F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_payment_data (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', 
        payment_data_id VARCHAR(255) DEFAULT NULL, account_owner VARCHAR(255) NOT NULL, iban VARCHAR(255) NOT NULL, 
        PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E092EBCAFD6 FOREIGN KEY (payment_data_id) REFERENCES customer_payment_data (id)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException();
    }
}
